'''
This program illustrates the use of findContours and drawContours.
The original image is put up along with the image of drawn contours.

Usage:
    contours.py
A trackbar is put up which controls the contour level from -3 to 3
'''

import numpy as np
import cv2
import serial
import time


class Contour:
    """
	Main features for every contour found by cv2.findContours()
	A Trend line is built from the bottom of each contour and 
	a linear function is found out of it
    """
    def __init__(self,img,cnt):
        self.img = img
        self.cnt = cnt
	#Finding useful cnt attributes
        self.size = len(cnt)
	self.leftmost = tuple(self.cnt[self.cnt[:,:,0].argmin()][0])
        self.rightmost = tuple(self.cnt[self.cnt[:,:,0].argmax()][0])
        self.topmost = tuple(self.cnt[self.cnt[:,:,1].argmin()][0])
        self.bottommost = tuple(self.cnt[self.cnt[:,:,1].argmax()][0])
        self.extreme = (self.leftmost,self.rightmost,self.topmost,self.bottommost)
	self.list = list(self.cnt.__array__().tolist())
	self.list = [tuple(element[0]) for element in self.list]
	self.leftindex = self.list.index(self.leftmost)
	self.rightindex = self.list.index(self.rightmost)
	self.topindex = self.list.index(self.topmost)
	self.bottomindex = self.list.index(self.bottommost)
        self.convex_hull = cv2.convexHull(cnt)
        self.convex_area = cv2.contourArea(self.convex_hull)
	#Finding parameters for line equation
	self.trend_line = [(0,0),(0,0)]
	self.trend_slope = None
	self.yIntercept = None
	self.xIntercept = None
	self.line=[(0,0),(0,0)]
	
	if(self.bottomindex-10>=0):
		self.trend_line = [self.list[self.bottomindex],self.list[self.bottomindex-10]]
	else:
		self.trend_line = [self.list[self.bottomindex],self.list[self.topindex]]
	self.deltaX = self.trend_line[0][0]-self.trend_line[1][0]
	self.deltaY = (self.trend_line[0][1]-self.trend_line[1][1])
	h, w = img.shape[:2]
	if self.deltaX == 0:#Avoiding division by 0
		self.trend_slope = self.deltaY
		self.line=[(self.trend_line[0][0],0),(self.trend_line[0][0],h)]
		self.xIntercept=self.trend_line[0][0]
		
	else:
		self.trend_slope = self.deltaY/float(self.deltaX)
		self.yIntercept = self.trend_line[0][1]-self.trend_slope*self.trend_line[0][0]
		self.line=[(0,cv.Round(self.yIntercept)),(w,cv.Round(self.trend_slope*w+self.yIntercept))]
		if self.trend_slope==0:
			self.xIntercept=None
		else:
			self.xIntercept=cv.Round((h-self.yIntercept)/float(self.trend_slope))
class Line:
    """
	Main features for every line, used by createLinesSet
	Every line is built by the vanishing point previously
	found and a the center of a mask of color 
	A line equation is calculated out of it and most importantly
	the xIntercept to locate lanes in the next steps
    """
    def __init__(self,vanishing,color,samplingHeight,sampleX):
	self.color=color
	self.samplingHeight=samplingHeight
	self.sampleX=sampleX
	self.trend_line = [(0,0),(0,0)]
	self.trend_slope = None
	self.yIntercept = None
	self.xIntercept = None
	self.line=[(0,0),(0,0)]
	self.trend_line = [(sampleX,samplingHeight),vanishing]
	self.deltaX = self.trend_line[0][0]-self.trend_line[1][0]
	self.deltaY = self.trend_line[0][1]-self.trend_line[1][1]
	h, w = img.shape[:2]
	if self.deltaX == 0:#Avoiding division by 0
		self.trend_slope = self.deltaY
		self.line=[(self.trend_line[0][0],0),(self.trend_line[0][0],h)]
		self.xIntercept=self.trend_line[0][0]
	else:
		self.trend_slope = self.deltaY/float(self.deltaX)
		self.yIntercept = self.trend_line[0][1]-self.trend_slope*self.trend_line[0][0]
		self.line=[(0,cv.Round(self.yIntercept)),(w,cv.Round(self.trend_slope*w+self.yIntercept))]
		if self.trend_slope==0:
			self.xIntercept=None
		else:
			self.xIntercept=cv.Round((h-self.yIntercept)/float(self.trend_slope))

    def plot(self,vis):
	"""
	Plot white lines with black color and yellow ones with red color
    	"""
	if self.color!="white":
		cv2.line(vis,self.line[0],self.line[1], (0,0,0), 1, cv2.CV_AA)
	else:
		cv2.line(vis,self.line[0],self.line[1], (0, 0, 255), 1, cv2.CV_AA)
	
	

def vanishing_point(contours):
	"""
	From a set of contours find a set of intercepts in the x and y directions
	by using the line equation of each one
	
    	"""
	xPoints = []
	yPoints = []
	for i in xrange(len(contours)-1):
		cnt=Contour(img,contours[i])
		for j in xrange(i+1,len(contours)):
			cntAux = Contour(img,contours[j])
			if cnt.trend_slope-cntAux.trend_slope != 0:#Avoiding division by 0
				if cnt.trend_slope*cntAux.trend_slope<0:#Allowing points from a positive and a negative slope intercept
					if cntAux.yIntercept==None and  cnt.yIntercept!=None:
						xintercept=cnt.xIntercept
						yintercept=cnt.yIntercept+xintercept*cnt.trend_slope
						xPoints.append(cv.Round(xintercept))
		 				yPoints.append(cv.Round(yintercept))
					elif cntAux.yIntercept!=None and  cnt.yIntercept==None:
						xintercept=cnt.xIntercept
						yintercept=cntAux.yIntercept+xintercept*cntAux.trend_slope
						xPoints.append(cv.Round(xintercept))
		 				yPoints.append(cv.Round(yintercept))
					elif cntAux.yIntercept==None and  cnt.yIntercept==None:
						pass
					else:
						xintercept=(cntAux.yIntercept-cnt.yIntercept)/float(cnt.trend_slope-cntAux.trend_slope)
						yintercept=cntAux.yIntercept+xintercept*cntAux.trend_slope
						xPoints.append(cv.Round(xintercept))
		 				yPoints.append(cv.Round(yintercept))
	return (xPoints,yPoints)

def median(mylist):
    """
	Statistics for finding the most accurate vanishing point
    """
    sorts = sorted(mylist)
    length = len(sorts)
    if not length % 2:
        return (sorts[length / 2] + sorts[length / 2 - 1]) / 2.0
    return sorts[length / 2]			
		
def weightedAverage(mylist,maxValue,partSize):
	"""
	Sections are defined and values are weighted by the number 
	of components found in its partition
    	"""
	currentPoint = 0
	sumOverAll=0
	weights=0
	
	while (currentPoint+partSize)<maxValue:
		intervalCount=0
		listOfChosen = []
		comparison = currentPoint+partSize
		if currentPoint+partSize>maxValue : 
	        	comparison = maxValue

		for item in mylist:
			if currentPoint<item<=comparison:
				intervalCount=intervalCount+1;
				listOfChosen.append(item)
		sumOverAll = intervalCount*sum(listOfChosen)+sumOverAll
		weights= intervalCount * intervalCount + weights
		currentPoint = comparison

	return sumOverAll/float(weights)

def returnOnesIndexes(ones):
	white=[]
	for index in xrange(len(ones)):
		if ones[index] == 1:
			white.append(index)
	return white

def thresholds(r,g,b):
	"""
	Look for levels of colors and return a couple of lists
	of ones pointing the candidates for a white line or a yellow one
    	"""
	ro=[1 if r[element]>=170 else 0 for element in xrange(len(r))]
	go=[1 if g[element]>=170 else 0 for element in xrange(len(g))] 
	bo=[1 if b[element]>=170 else 0 for element in xrange(len(b))] 
	dot =[ro[element]*go[element]*bo[element] for element in xrange(len(g))]
	white=returnOnesIndexes(dot)
	#red = [ro[element]-go[element]-bo[element] for element in xrange(len(ro))]
	red = [ro[element]-bo[element] for element in xrange(len(ro))]
	red = [1 if red[element]==1 else 0 for element in xrange(len(red))]
	
	red = returnOnesIndexes(red)
	
	return (white,red)

def interceptContour(img,cnt,height):
	#[1 if red[element]==1 else 0 for element in xrange(len(red))]
	yList = [cnt.list[i][1] for i in xrange(len(cnt.list))]
	listDiff = [ yItem-height for yItem in yList]
	listMinus=[abs(item) if item<=0 else float("inf") for item in listDiff]
	listPlus=[ item if item>=0 else float("inf") for item in listDiff]
	overIdx  = listMinus.index(min(listMinus)) 
	belowIdx = listPlus.index(min(listPlus))
	overPoint = cnt.list[overIdx]
	belowPoint = cnt.list[belowIdx]
	if (overPoint[1] > height and belowPoint[1] > height) or (overPoint[1] < height and belowPoint[1] < height):
	    return None
	averageIntercept = (overPoint[0]+belowPoint[0])/2
	cv2.circle(img,(overPoint), 10, (0, 255, 255))
	cv2.circle(img,(belowPoint), 10, (255, 0, 255))
	cv2.circle(img,(averageIntercept,height), 10, (0, 0, 0))
	return (averageIntercept,height)


def interceptsGivenH(img,contours,height):
	intercepts=[]
	for cnt in contours:
	    tupla=interceptContour(img,cnt,height)
	    if tupla!=None:
		intercepts.append(tupla)
	intercepts = sorted(intercepts, key=lambda x: x[0], reverse=False)
	return intercepts



def createLines(white,color):
	"""
	From a given list of ones, find the average index of the spot and 
	create a line which is represented by a tuple containing 
	its averge and its color
    	"""
	currentWhite=[]
	whiteLines=[]
	itemCount=0
	for i in xrange(len(white)):
		if itemCount==0:
			currentWhite.append(white[i])
			itemCount=1;

		elif white[i-1]==white[i]-1:
			currentWhite.append(white[i])
			itemCount=itemCount+1;
			if i==len(white):
				whiteLines.append((color,sum(currentWhite)/len(currentWhite)))
		else:
			whiteLines.append((color,sum(currentWhite)/len(currentWhite)))
			itemCount=0
			currentWhite=[]
			currentWhite.append(white[i])
		if i==len(white)-1:
			whiteLines.append((color,sum(currentWhite)/len(currentWhite)))
	return whiteLines

def createLinesSet(vanishing,lines,samplingHeight,img):
	"""
	From a sample and the vanishing point built a Line object 
    	"""
	lineSet=[]
	for line in lines:
		currentLine=Line(vanishing,line[0],samplingHeight,line[1])
		if currentLine.xIntercept!=None:
			lineSet.append(currentLine)
			currentLine.plot(img)
	return lineSet
			
def ignoreFalseLines(white,yellow):
	"""
	Ignore a yellow line if its too close to a white line
    	"""
	for whiteLine in white:
		for yellowLine in yellow:
			difference=abs(whiteLine[1]-yellowLine[1])
			if(difference<100):
				yellow.remove(yellowLine)
def createLaneSet(lineSet):
	"""
	Mix all lines found in all possible combinations to make a set of lines
    	"""
	laneSet=[]
	for i in xrange(len(lineSet)-1):
		line=lineSet[i]
		for j in xrange(i+1,len(lineSet)):
			lineAux = lineSet[j]
			if line.xIntercept<lineAux.xIntercept:
				lane = (line.color,lineAux.color,(line.xIntercept+lineAux.xIntercept)/2)
			else:
				lane = (lineAux.color,line.color,(line.xIntercept+lineAux.xIntercept)/2)
			laneSet.append(lane)
	return laneSet

def makeDecision(lane,w):
	"""
	Check x intercepts and decide whether the car needs to move left or right 
	with a given figure of deviation
    	"""
	deviation=w/2-lane[2]
	
		
	if deviation<0:
		return ("right",abs(deviation))
	else:
		return ("left",abs(deviation))

def makeSimpleDecision(tty,currentX,w,previousState):
	"""
	Check x intercepts and decide whether the car needs to move left or right 
	with a given figure of deviation
    	"""
	state="Stop"
	if currentX==None:
		tty.write(chr(0x00)+chr(0x00));
		return None , state
	
	deviation=w/2-currentX
	
	if abs(deviation)<30:
		state="Forward"
		if(previousState!="Forward"):
			tty.write(chr(0xFF)+chr(0xFF)); time.sleep(0.01);tty.write(chr(0x60)+chr(0x60));
		else:
			tty.write(chr(0x60)+chr(0x60));
		
	elif deviation<0:
		state="Right"
		if(previousState=="Stop"):
			tty.write(chr(0xFF)+chr(0x00)); time.sleep(0.01);tty.write(chr(0x60)+chr(0x00));
		else:
			if abs(deviation)>120:
				tty.write(chr(120)+chr(0x60));
			else:
				tty.write(chr(120)+chr(0x30));
		
	elif deviation>0:
		state="Left"
		if(previousState=="Stop"):
			tty.write(chr(0x00)+chr(0xFF)); time.sleep(0.01);tty.write(chr(0x00)+chr(0x60));
		else:
			if abs(deviation)>120:
				tty.write(chr(0x60)+chr(120));
			else:
				tty.write(chr(0x30)+chr(120));
	if deviation<0:
		return ("right",abs(deviation)),state
	else:
		return ("left",abs(deviation)),state
		
				
if __name__ == '__main__':
    print __doc__
    import cv2.cv as cv
    from matplotlib import pyplot as plt

    tty = serial.Serial(port='/dev/ttyUSB0', baudrate=57600, timeout=1)
    c = cv2.VideoCapture(1)
    _,img = c.read()
    h, w = img.shape[:2]
    black, white = 0, 255
    samplingHeight=h/2
    sampHeightLow=3*h/4
    sampHeightHigh=h/4
    kalman = cv.CreateKalman(4, 2, 0)
    kalman_state = cv.CreateMat(4, 1, cv.CV_32FC1)
    kalman_process_noise = cv.CreateMat(4, 1, cv.CV_32FC1)
    kalman_measurement = cv.CreateMat(2, 1, cv.CV_32FC1)
    previousState="Stop"

    print dir(kalman)
    
    while True:
	    
	    #img = make_image()
	    
	    s,img = c.read()
	    cv2.bitwise_not(img, img)
	    #img = cv2.multiply(img,np.array([1.2]))  
	    #img = cv2.imread('../c/railfollower/capture.jpg')
	    if s:
		    imag = img.copy()
		    imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
		    #imgray=cv2.bilateralFilter(imgray,1, 1*2,1/2)
		    #imgray=cv2.GaussianBlur(imgray, (3, 3), 0)
		    imgray = cv2.blur(imgray,(3,3))
		    
		    
		    #ret,imgray = cv2.threshold(imgray,127,255,0)
		    #ret,imgray = cv2.threshold(imgray,80,255,0)
		    thresh=cv2.Canny(imgray, 100, 2*100)
		    #img=thresh
		    laneSet=[]

		    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

		    contours = sorted(contours, key=lambda x: x.size, reverse=True)
		    cv2.drawContours(img,contours,-1,(0,255,0),3)

	 	    lowB = list(img[sampHeightLow,0:w,0].__array__().tolist())
	            lowG = list(img[sampHeightLow,0:w,1].__array__().tolist())
	            lowR = list(img[sampHeightLow,0:w,2].__array__().tolist())

		    highB = list(img[sampHeightHigh,0:w,0].__array__().tolist())
		    highG = list(img[sampHeightHigh,0:w,1].__array__().tolist())
	            highR = list(img[sampHeightHigh,0:w,2].__array__().tolist())

		    listaB = list(img[samplingHeight,0:w,0].__array__().tolist())
		    listaG = list(img[samplingHeight,0:w,1].__array__().tolist())
		    listaR = list(img[samplingHeight,0:w,2].__array__().tolist())

		    whiteSpots,redSpots=thresholds(listaR,listaG,listaB)
		    whiteLines=createLines(whiteSpots,"white")
		    #redLines=createLines(redSpots,"yellow")
		    redLines=[]
		    ignoreFalseLines(whiteLines,redLines)
		    #print "Lines Half ", whiteLines,redLines

		    whiteLow,redLow=thresholds(lowR,lowG,lowB)
		    whiteLinesL=createLines(whiteLow,"white")
		    #redLinesL=createLines(redLow,"yellow")
		    redLinesL=[]
		    ignoreFalseLines(whiteLinesL,redLinesL)
		    #print "Lines Low ", whiteLinesL,redLinesL
			
		    whiteHigh,redHigh=thresholds(highR,highG,highB)
		    whiteLinesH=createLines(whiteHigh,"white")
		    #redLinesH=createLines(redHigh,"yellow")
		    redLinesH=[]
		    ignoreFalseLines(whiteLinesH,redLinesH)
		    #print "Lines High ", whiteLinesH,redLinesH


		    del contours[5:]
		    Advance=False
		    
		    if len(contours)>=2:
			Advance=True
			listOfCnt=[Contour(img,contours[i]) for i in xrange(len(contours))]
			intercepts=interceptsGivenH(img,listOfCnt,samplingHeight)
			#print intercepts
			if len(intercepts)!=0:
				extremes=[intercepts[0][0],intercepts[len(intercepts)-1][0]]
				currentX=sum(extremes)/len(extremes)
				cv2.line(img,(currentX,0),(currentX,h), (10, 255, 155), 1, cv2.CV_AA)
				decision, previousState = makeSimpleDecision(tty,currentX,w,previousState)
				cv2.putText(img,decision[0]+" "+decision[1].__str__(),(50,50), cv2.FONT_HERSHEY_PLAIN, 2.0,(0,0,0))
			else:
				decision, previousState=makeSimpleDecision(tty,None,w,previousState)
			
			

			cv2.line(img,(w/2,0),(w/2,h), (0,0,0), 1, cv2.CV_AA)
		    else:
			decision, previousState=makeSimpleDecision(tty,None,w,previousState)
		    print previousState
			
		    if Advance:
			    xPoints,yPoints=vanishing_point(contours)
	    		    if len(xPoints)==0 or len(yPoints)==0:
				Advance=False
		    if Advance:		
			    averageX = cv.Round(median(xPoints))
	 		    averageY = cv.Round(median(yPoints))
			    #print "yPoints ",yPoints
			    #print "xPoints ",xPoints

			    
			    # set previous state for prediction
			    kalman.state_pre[0,0]  = averageX
			    kalman.state_pre[1,0]  = averageY
			    kalman.state_pre[2,0]  = 0
			    kalman.state_pre[3,0]  = 0

			    # set kalman transition matrix
			    kalman.transition_matrix[0,0] = 1
			    kalman.transition_matrix[0,1] = 0
			    kalman.transition_matrix[0,2] = 0
			    kalman.transition_matrix[0,3] = 0
			    kalman.transition_matrix[1,0] = 0
			    kalman.transition_matrix[1,1] = 1
			    kalman.transition_matrix[1,2] = 0
			    kalman.transition_matrix[1,3] = 0
			    kalman.transition_matrix[2,0] = 0
			    kalman.transition_matrix[2,1] = 0
			    kalman.transition_matrix[2,2] = 0
			    kalman.transition_matrix[2,3] = 1
			    kalman.transition_matrix[3,0] = 0
			    kalman.transition_matrix[3,1] = 0
			    kalman.transition_matrix[3,2] = 0
			    kalman.transition_matrix[3,3] = 1

			    # set Kalman Filter
			    cv.SetIdentity(kalman.measurement_matrix, cv.RealScalar(1))
			    cv.SetIdentity(kalman.process_noise_cov, cv.RealScalar(1e-5))
			    cv.SetIdentity(kalman.measurement_noise_cov, cv.RealScalar(1e-5))
			    cv.SetIdentity(kalman.error_cov_post, cv.RealScalar(1))

			    kalman_prediction = cv.KalmanPredict(kalman)
		 	    predict_pt  = (kalman_prediction[0,0], kalman_prediction[1,0])
			    
			    kalman_estimated = cv.KalmanCorrect(kalman, kalman_measurement)
			    state_pt = (kalman_estimated[0,0], kalman_estimated[1,0])
			    #print "state_pt", (state_pt), "predict_pt ",predict_pt

			    kalman_measurement[0, 0] = averageX
			    kalman_measurement[1, 0] = averageY

			    lineSet=createLinesSet((averageX,averageY),whiteLines+redLines,samplingHeight,img)
			    laneSet=createLaneSet(lineSet)
			    #cv2.circle(img,(averageX,averageY), 20, (255, 255, 255))
			    #cv2.circle(img,(cv.Round(state_pt[0]),cv.Round(state_pt[1])), 20, (255, 0, 0))
			    #cv2.circle(img,(cv.Round(predict_pt[0]),cv.Round(predict_pt[1])), 20, (0, 255, 0))

 			    #cv2.line(img,(averageX,0),(averageX,h), (100, 100, 255), 3, cv2.CV_AA)
			    #cv2.line(img,(0,averageY),(w,averageY), (100, 255, 100), 3, cv2.CV_AA)
			    #cv2.line(img,(0,wAverage),(w,wAverage), (255, 100, 100), 3, cv2.CV_AA)
		    if len(laneSet)!=0:	
		    	decision = makeDecision(laneSet[0],w)
		    	cv2.putText(img,decision[0]+" "+decision[1].__str__(),(20,20), cv2.FONT_HERSHEY_PLAIN, 1.0,(0,255,0))
		    else:
		     	cv2.putText(img,"No lanes detected",(20,20), cv2.FONT_HERSHEY_PLAIN, 1.0,(0,255,0))
		    cv2.line(img,(0,samplingHeight),(w,samplingHeight), (0, 0, 0), 2, cv2.CV_AA)  
		    cv2.line(img,(0,sampHeightLow),(w,sampHeightLow), (50, 0, 0), 2, cv2.CV_AA)  
                    cv2.line(img,(0,sampHeightHigh),(w,sampHeightHigh), (0, 50, 0), 2, cv2.CV_AA)  

		    cv2.imshow('image', img)
		    k = cv2.waitKey(20)
	 
		    if k == 27:
			tty.write(chr(0x00)+chr(0x00))
			break
    cv2.destroyAllWindows() 			
